﻿using WinUSBNet.Detector;

namespace WinUSBNet
{
    /// <summary>
    /// Gives information about a device. This information is retrieved using the setup API, not the
    /// actual device descriptor. Device description and manufacturer will be the strings specified
    /// in the .inf file. After a device is opened the actual device descriptor can be read as well.
    /// </summary>
    public class USBDeviceInfo
    {
        private DetectorDeviceInfo _details;

        /// <summary>
        /// Vendor ID (VID) of the USB device
        /// </summary>
        public int VID
        {
            get
            {
                return _details.VendorId;
            }
        }

        /// <summary>
        /// Product ID (VID) of the USB device
        /// </summary>
        public int PID
        {
            get
            {
                return _details.ProductId;
            }
        }

        /// <summary>
        /// Manufacturer of the device, as specified in the INF file (not the device descriptor)
        /// </summary>
        public string Manufacturer
        {
            get
            {
                return _details.Manufacturer;
            }
        }

        /// <summary>
        /// Description of the device, as specified in the INF file (not the device descriptor)
        /// </summary>
        public string DeviceDescription
        {
            get
            {
                return _details.Description;
            }
        }

        /// <summary>
        /// Device pathname
        /// </summary>
        public string DevicePath
        {
            get
            {
                return _details.DevicePath;
            }
        }

        internal USBDeviceInfo(DetectorDeviceInfo details)
        {
            _details = details;
        }

    }
}
