﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinUSBNet
{
    /// <summary>
    /// USB base class code enumeration, as defined in the USB specification
    /// </summary>
    public enum USBBaseClass
    {
        Unknown = -1,                    //Unknown non-zero class code. Used when the actual class code does not match any of the ones defined in this enumeration.
        None = 0x00,                     //Base class defined elsewhere (0x00)
        Audio = 0x01,                    //Audio base class (0x01)
        CommCDC = 0x02,                  //Communications and CDC control base class (0x02)
        HID = 0x03,                      //HID base class (0x03)
        Physical = 0x05,                 //Physical base class (0x05)
        Image = 0x06,                    //Image base class (0x06)
        Printer = 0x07,                  //Printer base class (0x07)
        MassStorage = 0x08,              //Mass storage base class (0x08)
        Hub = 0x09,                      //Hub base class (0x09)
        CDCData = 0x0A,                  //CDC data base class (0x0A)
        SmartCard = 0x0B,                //Smart card base class (0x0B)
        ContentSecurity = 0x0D,          //Content security base class (0x0D)
        Video = 0x0E,                    //Video base class (0x0E)
        PersonalHealthcare = 0x0F,       //Personal health care base class (0x0F)
        DiagnosticDevice = 0xDC,         //Diagnostic device base class (0xDC)
        WirelessController = 0xE0,       //Wireless controller base class (0xE0)
        Miscellaneous = 0xEF,            //Miscellaneous base class (0xEF)
        ApplicationSpecific = 0xFE,      //Application specific base class (0xFE)
        VendorSpecific = 0xFF,           //Vendor specific base class (0xFF)
    }


    public class USBDeviceDescriptor
    {
        /// <summary>
        /// Windows path name for the USB device
        /// </summary>
        public string PathName { get; private set; }

        /// <summary>
        /// USB vendor ID (VID) of the device
        /// </summary>
        public int VID { get; private set; }

        /// <summary>
        /// USB product ID (PID) of the device
        /// </summary>
        public int PID { get; private set; }

        /// <summary>
        /// Manufacturer name, or null if not available
        /// </summary>
        public string Manufacturer { get; private set; }

        /// <summary>
        /// Product name, or null if not available
        /// </summary>
        public string Product { get; private set; }

        /// <summary>
        /// Device serial number, or null if not available
        /// </summary>
        public string SerialNumber { get; private set; }


        /// <summary>
        /// Friendly device name, or path name when no
        /// further device information is available
        /// </summary>
        public string FullName
        {
            get
            {
                if (Manufacturer != null && Product != null)
                {
                    return Product + " - " + Manufacturer;
                }
                else if (Product != null)
                {
                    return Product;
                }
                else if (SerialNumber != null)
                {
                    return SerialNumber;
                }
                else
                {
                    return PathName;
                }
            }
        }

        /// <summary>
        /// Device class code as defined in the interface descriptor
        /// This property can be used if the class type is not defined
        /// int the USBBaseClass enumeration
        /// </summary>
        public byte ClassValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Device subclass code
        /// </summary>
        public byte SubClass
        {
            get;
            private set;
        }

        /// <summary>
        /// Device protocol code
        /// </summary>
        public byte Protocol
        {
            get;
            private set;
        }

        /// <summary>
        /// Device class code. If the device class does
        /// not match any of the USBBaseClass enumeration values
        /// the value will be USBBaseClass.Unknown
        /// </summary>
        public USBBaseClass BaseClass
        {
            get;
            private set;
        }

        internal USBDeviceDescriptor(string path, USB_DEVICE_DESCRIPTOR deviceDesc, string manufacturer, string product, string serialNumber)
        {
            PathName = path;
            VID = deviceDesc.idVendor;
            PID = deviceDesc.idProduct;
            Manufacturer = manufacturer;
            Product = product;
            SerialNumber = serialNumber;


            ClassValue = deviceDesc.bDeviceClass;
            SubClass = deviceDesc.bDeviceSubClass;
            Protocol = deviceDesc.bDeviceProtocol;

            // If interface class is of a known type (USBBaseeClass enum), use this
            // for the InterfaceClass property.
            BaseClass = USBBaseClass.Unknown;
            if (Enum.IsDefined(typeof(USBBaseClass), (int)deviceDesc.bDeviceClass))
            {
                BaseClass = (USBBaseClass)(int)deviceDesc.bDeviceClass;
            }
        }
    }
}
