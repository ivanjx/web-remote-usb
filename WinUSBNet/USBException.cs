﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WinUSBNet
{
    /// <summary>
    /// Exception used by WinUSBNet to indicate errors. This is the
    /// main exception to catch when using the library.
    /// </summary>
    public class USBException : Exception
    {
        /// <summary>
        /// Constructs a new USBException with the given message
        /// </summary>
        /// <param name="message">The message describing the exception</param>
        public USBException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructs a new USBException with the given message and underlying exception
        /// that caused the USBException.
        /// </summary>
        /// <param name="message">The message describing the exception</param>
        /// <param name="innerException">The underlying exception causing the USBException</param>
        public USBException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }


    /// <summary>
    /// Exception used internally to catch Win32 API errors. This exception should
    /// not be thrown to the library's caller.
    /// </summary>
    internal class APIException : Exception
    {
        public APIException(string message) :
            base(message)
        {
        }

        public APIException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public static APIException Win32(string message)
        {
            return APIException.Win32(message, Marshal.GetLastWin32Error());
        }

        public static APIException Win32(string message, int errorCode)
        {
            return new APIException(message, new Win32Exception(errorCode));

        }
    }
}
