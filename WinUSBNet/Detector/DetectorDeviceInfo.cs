﻿
using System;

namespace WinUSBNet.Detector
{
    public struct DetectorDeviceInfo
    {
        public string DevicePath;
        public string Manufacturer;
        public string Description;
        public ushort VendorId;
        public ushort ProductId;
    }
}
