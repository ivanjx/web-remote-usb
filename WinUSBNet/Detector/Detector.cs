﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;

namespace WinUSBNet.Detector
{
    public partial class Detector 
    {
        private const Int32 DIGCF_PRESENT = 2;
        private const Int32 DIGCF_DEVICEINTERFACE = 0X10;
        private const int ERROR_NO_MORE_ITEMS = 259;
        private const int ERROR_INSUFFICIENT_BUFFER = 122;
        private static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        private enum RegTypes : int
        {
            REG_SZ = 1,
            REG_MULTI_SZ = 7
        }
                

        public static DetectorDeviceInfo[] FindDevicesFromGuid(string guidString)
        {
            List<DetectorDeviceInfo> deviceList = new List<DetectorDeviceInfo>();

            Guid guid = new(guidString);

            IntPtr deviceInfoSet = IntPtr.Zero;

            try
            {
                deviceInfoSet = API.SetupDiGetClassDevs(ref guid, IntPtr.Zero, IntPtr.Zero, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

                if (deviceInfoSet == INVALID_HANDLE_VALUE)
                {
                    throw new Exception("Failed to enumerate devices.");
                }

                int memberIndex = 0;

                while (true)
                {
                    // Begin with 0 and increment through the device information set until
                    // no more devices are available.
                    API.SP_DEVICE_INTERFACE_DATA deviceInterfaceData = new API.SP_DEVICE_INTERFACE_DATA();

                    // The cbSize element of the deviceInterfaceData structure must be set to
                    // the structure's size in bytes.
                    // The size is 28 bytes for 32-bit code and 32 bytes for 64-bit code.
                    deviceInterfaceData.cbSize = Marshal.SizeOf(deviceInterfaceData);

                    bool success;

                    success = API.SetupDiEnumDeviceInterfaces(deviceInfoSet, IntPtr.Zero, ref guid, memberIndex, ref deviceInterfaceData);

                    // Find out if a device information set was retrieved.
                    if (!success)
                    {
                        int lastError = Marshal.GetLastWin32Error();

                        if (lastError == ERROR_NO_MORE_ITEMS)
                        {
                            break;
                        }

                        throw new Exception("Failed to get device interface.");
                    }
                    // A device is present.

                    int bufferSize = 0;

                    success = API.SetupDiGetDeviceInterfaceDetail(
                        deviceInfoSet, 
                        ref deviceInterfaceData, 
                        IntPtr.Zero,
                        0,
                        ref bufferSize,
                        IntPtr.Zero);

                    if (!success)
                    {
                        if (Marshal.GetLastWin32Error() != ERROR_INSUFFICIENT_BUFFER)
                        {
                            throw new Exception("Failed to get interface details buffer size.");
                        }
                    }

                    IntPtr detailDataBuffer = IntPtr.Zero;

                    try
                    {
                        // Allocate memory for the SP_DEVICE_INTERFACE_DETAIL_DATA structure using the returned buffer size.
                        detailDataBuffer = Marshal.AllocHGlobal(bufferSize);

                        // Store cbSize in the first bytes of the array. The number of bytes varies with 32- and 64-bit systems.
                        Marshal.WriteInt32(detailDataBuffer, (IntPtr.Size == 4) ? (4 + Marshal.SystemDefaultCharSize) : 8);

                        // Call SetupDiGetDeviceInterfaceDetail again.
                        // This time, pass a pointer to DetailDataBuffer
                        // and the returned required buffer size.

                        // build a DevInfo Data structure
                        API.SP_DEVINFO_DATA da = new API.SP_DEVINFO_DATA();

                        da.cbSize = Marshal.SizeOf(da);

                        success = API.SetupDiGetDeviceInterfaceDetail(deviceInfoSet, ref deviceInterfaceData, detailDataBuffer, bufferSize, ref bufferSize, ref da);

                        if (!success)
                        {
                            throw new Exception("Failed to get device interface details.");
                        }


                        // Skip over cbsize (4 bytes) to get the address of the devicePathName.

                        IntPtr pDevicePathName = new IntPtr(detailDataBuffer.ToInt64() + 4);
                        string pathName = Marshal.PtrToStringUni(pDevicePathName)!;

                        // Get the String containing the devicePathName.
                        DetectorDeviceInfo device = GetDevDetails(pathName, deviceInfoSet, da);

                        deviceList.Add(device);
                    }
                    finally
                    {
                        if (detailDataBuffer != IntPtr.Zero)
                        {
                            Marshal.FreeHGlobal(detailDataBuffer);
                            detailDataBuffer = IntPtr.Zero;
                        }
                    }

                    memberIndex++;
                }
            }
            finally
            {
                if (deviceInfoSet != IntPtr.Zero && deviceInfoSet != INVALID_HANDLE_VALUE)
                {
                    API.SetupDiDestroyDeviceInfoList(deviceInfoSet);
                }
            }

            return deviceList.ToArray();
        }

        private static DetectorDeviceInfo GetDevDetails(string devicePath, IntPtr deviceInfoSet, API.SP_DEVINFO_DATA deviceInfoData)
        {
            DetectorDeviceInfo device = new DetectorDeviceInfo();

            device.DevicePath = devicePath;
            device.Description = GetStringProperty(deviceInfoSet, deviceInfoData, API.SPDRP.SPDRP_DEVICEDESC);
            device.Manufacturer = GetStringProperty(deviceInfoSet, deviceInfoData, API.SPDRP.SPDRP_MFG);
            string[] hardwareIDs = GetMultiStringProperty(deviceInfoSet, deviceInfoData, API.SPDRP.SPDRP_HARDWAREID);

            bool foundVidPid = false;

            foreach (string hardwareID in hardwareIDs)
            {
                Match match = UsbRegex().Match(hardwareID);

                if (match.Success)
                {
                    device.VendorId = ushort.Parse(match.Groups[1].Value, NumberStyles.AllowHexSpecifier);
                    device.ProductId = ushort.Parse(match.Groups[2].Value, NumberStyles.AllowHexSpecifier);

                    foundVidPid = true;

                    break;
                }
            }

            if (!foundVidPid)
            {
                throw new Exception("Failed to find VID and PID for USB device. No hardware ID could be parsed.");
            }

            return device;
        }


        // todo: is the queried data always available, or should we check ERROR_INVALID_DATA?
        private static string GetStringProperty(IntPtr deviceInfoSet, API.SP_DEVINFO_DATA deviceInfoData, API.SPDRP property)
        {
            int regType;
            byte[] buffer = GetProperty(deviceInfoSet, deviceInfoData, property, out regType);
            if (regType != (int)RegTypes.REG_SZ)
            {
                throw new Exception("Invalid registry type returned for device property.");
            }

            // sizof(char), 2 bytes, are removed to leave out the string terminator
            return Encoding.Unicode.GetString(buffer, 0, buffer.Length - sizeof(char));
        }

        private static string[] GetMultiStringProperty(IntPtr deviceInfoSet, API.SP_DEVINFO_DATA deviceInfoData, API.SPDRP property)
        {
            int regType;
            byte[] buffer = GetProperty(deviceInfoSet, deviceInfoData, property, out regType);
            if (regType != (int)RegTypes.REG_MULTI_SZ)
            {
                throw new Exception("Invalid registry type returned for device property.");
            }

            string fullString = Encoding.Unicode.GetString(buffer);

            return fullString.Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);

        }

        private static byte[] GetProperty(IntPtr deviceInfoSet, API.SP_DEVINFO_DATA deviceInfoData, API.SPDRP property, out int regType)
        {
            uint requiredSize;

            if (!API.SetupDiGetDeviceRegistryProperty(deviceInfoSet, ref deviceInfoData, property, IntPtr.Zero, IntPtr.Zero, 0, out requiredSize))
            {
                if (Marshal.GetLastWin32Error() != ERROR_INSUFFICIENT_BUFFER)
                {
                    throw new Exception("Failed to get buffer size for device registry property.");
                }
            }

            byte[] buffer = new byte[requiredSize];

            if (!API.SetupDiGetDeviceRegistryProperty(deviceInfoSet, ref deviceInfoData, property, out regType, buffer, (uint)buffer.Length, out requiredSize))
            {
                throw new Exception("Invalid registry type returned for device property.");
            }

            return buffer;
        }

        [GeneratedRegex("^USB\\\\VID_([0-9A-F]{4})&PID_([0-9A-F]{4})", RegexOptions.IgnoreCase)]
        private static partial Regex UsbRegex();
    }
}
