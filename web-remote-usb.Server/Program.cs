using System;
using System.IO.Ports;
using System.Threading;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using web_remote_usb.Server.Controllers;
using web_remote_usb.Server.Services;

const string LISTEN_ENDPOINT = "http://localhost:9002";

// Serilog.
var loggerConfiguration = new LoggerConfiguration()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
    .MinimumLevel.Override("System", LogEventLevel.Error)
    .Enrich.FromLogContext();

string template = "[{Timestamp:u} {Level:u3}] [{ConnectionId}] [{SourceContext}] {Message:lj}{NewLine}{Exception}";
loggerConfiguration = loggerConfiguration
    .WriteTo.Console(outputTemplate: template);

Log.Logger = loggerConfiguration.CreateLogger();

try
{
    WebApplicationBuilder builder = WebApplication.CreateSlimBuilder(args);
    builder.WebHost.UseUrls(LISTEN_ENDPOINT);
    builder.Host.UseSerilog();

    builder.Services
        // Controllers json serializer context.
        .ConfigureHttpJsonOptions(options =>
        {
            options.SerializerOptions.TypeInfoResolverChain.Insert(
                0,
                ControllerJsonContext.Default);
        })

        // Register built-in/libraries services.
        .AddCors()
        .AddSerilog(Log.Logger)

        // Register custom services.
        .AddSingleton<IHandleService<SerialPort>, HandleService<SerialPort>>()
        .AddSingleton<IHandleService<UsbDevice>, HandleService<UsbDevice>>()
        .AddSingleton<ISerialPortService, SerialPortService>()
        .AddSingleton<IUsbService, UsbService>()
        
        // Middleware
        .AddTransient<ExceptionHandlerMiddleware>();

    WebApplication application = builder.Build();
    application
        // Request logging.
        .UseSerilogRequestLogging()

        // CORS.
        .UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .SetIsOriginAllowed(origin =>
                origin.Contains("localhost"))) // Allow from localhost.

        // Exception handlers (from bottom -> top).
        .UseMiddleware<ExceptionHandlerMiddleware>()

        // Routing.
        .UseRouting();
    
    using CancellationTokenSource cts = new CancellationTokenSource();
    AppDomain.CurrentDomain.ProcessExit += (_, __) => cts.Cancel();

    // Controllers.
    IndexController.Map(application);
    SerialPortController.Map(application);
    UsbController.Map(application);
    ResetController.Map(application);
    
    // Done.
    Log.Information(
        "Listening on: {0}",
        LISTEN_ENDPOINT);
    await application.RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Server error");
}
finally
{
    Log.CloseAndFlush();
}
