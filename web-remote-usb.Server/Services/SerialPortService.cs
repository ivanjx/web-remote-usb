using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace web_remote_usb.Server.Services;

public interface ISerialPortService
{
    Task<string[]> ListAsync();
    Task<string> ConnectAsync(
        string name,
        int baudRate,
        int timeout);
    Task<byte[]> ReadAsync(
        string handle,
        int length,
        CancellationToken cancellationToken);
    Task WriteAsync(
        string handle,
        byte[] data,
        CancellationToken cancellationToken);
    void Disconnect(string handle);
    void DisconnectAll();
}

public class SerialPortService : ISerialPortService
{
    IHandleService<SerialPort> m_handleService;
    
    ILogger Log
    {
        get => Serilog.Log.Logger.ForContext<SerialPortService>();
    }

    public SerialPortService(
        IHandleService<SerialPort> handleService)
    {
        m_handleService = handleService;
    }

    public Task<string[]> ListAsync()
    {
        return Task.FromResult(
            SerialPort.GetPortNames());
    }

    public Task<string> ConnectAsync(string name, int baudRate, int timeout)
    {
        Log.Information("Opening port: {0}", name);
        SerialPort port = new SerialPort();
        port.PortName = name;
        port.BaudRate = baudRate;
        port.ReadTimeout = timeout;
        port.WriteTimeout = timeout;
        port.Open();

        Log.Information("Registering port handle: {0}", name);
        string handle = m_handleService.SetObject(port);
        return Task.FromResult(handle);
    }

    public void Disconnect(string handle)
    {
        SerialPort port = m_handleService.GetObject(handle);
        m_handleService.RemoveObject(handle);

        port.Close();
        Log.Information("Port connection closed: {0}", port.PortName);
    }

    public async Task<byte[]> ReadAsync(string handle, int length, CancellationToken cancellationToken)
    {
        SerialPort port = m_handleService.GetObject(handle);
        Log.Information("Reading {1} bytes from port {0}", port.PortName, length);

        using CancellationTokenRegistration reg = cancellationToken.Register(() =>
            port.DiscardInBuffer()); // Cancels port read.
        
        byte[] buffer = new byte[length];
        Task<int> readTask = port.BaseStream.ReadAsync(
            buffer,
            cancellationToken)
            .AsTask();
        Task delayTask = Task.Delay(port.ReadTimeout);
        
        try
        {
            await Task.WhenAny(readTask, delayTask);

            if (!readTask.IsCompleted)
            {
                Log.Information("Port reading timeout after {0} ms", port.ReadTimeout);
                port.DiscardInBuffer(); // Cancels port read.
                throw new TimeoutException();
            }

            int read = await readTask;
            
            if (read == length)
            {
                return buffer;
            }

            byte[] result = new byte[read];
            buffer.AsSpan()
                .Slice(0, read)
                .CopyTo(result);
            return result;
        }
        catch (IOException)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                throw new TaskCanceledException();
            }

            throw;
        }
    }

    public async Task WriteAsync(string handle, byte[] data, CancellationToken cancellationToken)
    {
        SerialPort port = m_handleService.GetObject(handle);
        Log.Information("Writing {1} bytes to port {0}", port.PortName, data.Length);

        using CancellationTokenRegistration reg = cancellationToken.Register(() =>
            port.DiscardOutBuffer()); // Cancels port write.
        
        Task writeTask = port.BaseStream.WriteAsync(
            data,
            cancellationToken)
            .AsTask();
        Task delayTask = Task.Delay(port.WriteTimeout);
        
        try
        {
            await Task.WhenAny(writeTask, delayTask);

            if (!writeTask.IsCompleted)
            {
                Log.Information("Port writing timeout after {0} ms", port.WriteTimeout);
                port.DiscardOutBuffer(); // Cancels port write.
                throw new TimeoutException();
            }
        }
        catch (IOException)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                throw new TaskCanceledException();
            }

            throw;
        }
    }

    public void DisconnectAll()
    {
        foreach (string handle in m_handleService.List())
        {
            SerialPort port = m_handleService.GetObject(handle);
            m_handleService.RemoveObject(handle);
            port.Close();
            Log.Information("Port connection closed: {0}", port.PortName);
        }
    }
}
