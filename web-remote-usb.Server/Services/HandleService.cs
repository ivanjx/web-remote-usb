using System;
using System.Collections.Generic;
using System.Linq;

namespace web_remote_usb.Server.Services;

public interface IHandleService<T>
{
    string[] List();
    T GetObject(string handle);
    string SetObject(T obj);
    void RemoveObject(string handle);
}

public class HandleService<T> : IHandleService<T>
{
    Dictionary<string, T> m_dict;

    public HandleService()
    {
        m_dict = new Dictionary<string, T>();
    }

    public T GetObject(string handle)
    {
        if (!m_dict.TryGetValue(handle, out T? result))
        {
            throw new Exception("Invalid handle");
        }

        return result;
    }

    public string[] List()
    {
        return m_dict.Keys.ToArray();
    }

    public void RemoveObject(string handle)
    {
        m_dict.Remove(handle);
    }

    public string SetObject(T obj)
    {
        string handle = Guid.NewGuid().ToString();
        m_dict[handle] = obj;
        return handle;
    }
}
