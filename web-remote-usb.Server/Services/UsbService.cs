using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using WinUSBNet.Detector;

namespace web_remote_usb.Server.Services;

public record UsbDeviceInfo(
    string Path,
    ushort Vid,
    ushort Pid);

public interface IUsbService
{
    Task<UsbDeviceInfo[]> ListAsync(string classGuid);
    Task<string> ConnectAsync(
        string devicePath,
        int timeout);
    Task<byte[]> ReadAsync(
        string handle,
        int length,
        CancellationToken cancellationToken);
    Task WriteAsync(
        string handle,
        byte[] data,
        CancellationToken cancellationToken);
    void Disconnect(string handle);
    void DisconnectAll();
}

public class UsbService : IUsbService
{
    IHandleService<UsbDevice> m_handleService;
    
    ILogger Log
    {
        get => Serilog.Log.Logger.ForContext<SerialPortService>();
    }

    public UsbService(
        IHandleService<UsbDevice> handleService)
    {
        m_handleService = handleService;
    }

    public Task<UsbDeviceInfo[]> ListAsync(string classGuid)
    {
        UsbDeviceInfo[] result = Detector.FindDevicesFromGuid(classGuid)
            .Select(x =>
                new UsbDeviceInfo(
                    x.DevicePath,
                    x.VendorId,
                    x.ProductId))
            .ToArray();
        return Task.FromResult(result);
    }

    public Task<string> ConnectAsync(string devicePath, int timeout)
    {
        Log.Information("Opening usb device: {0}", devicePath);
        UsbDevice device = new UsbDevice(devicePath, timeout);
        device.Open();

        Log.Information("Registering usb device: {0}", devicePath);
        string handle = m_handleService.SetObject(device);
        return Task.FromResult(handle);
    }

    public void Disconnect(string handle)
    {
        UsbDevice device = m_handleService.GetObject(handle);
        m_handleService.RemoveObject(handle);

        device.Dispose();
        Log.Information("Usb device connection closed: {0}", device.DevicePath);
    }

    public async Task<byte[]> ReadAsync(string handle, int length, CancellationToken cancellationToken)
    {
        UsbDevice device = m_handleService.GetObject(handle);
        Log.Information("Reading {1} bytes from usb {0}", device.DevicePath, length);
        
        byte[] buffer = new byte[length];
        int read = await device.ReadAsync(buffer, cancellationToken);

        if (read == length)
        {
            return buffer;
        }

        byte[] result = new byte[read];
        buffer.AsSpan()
            .Slice(0, read)
            .CopyTo(result);
        return result;
    }

    public async Task WriteAsync(string handle, byte[] data, CancellationToken cancellationToken)
    {
        UsbDevice device = m_handleService.GetObject(handle);
        Log.Information("Writing {1} bytes to usb {0}", device.DevicePath, data.Length);

        await device.WriteAsync(data, cancellationToken);
    }

    public void DisconnectAll()
    {
        foreach (string handle in m_handleService.List())
        {
            UsbDevice usb = m_handleService.GetObject(handle);
            m_handleService.RemoveObject(handle);
            usb.Dispose();
            Log.Information("Usb device connection closed: {0}", usb.DevicePath);
        }
    }
}
