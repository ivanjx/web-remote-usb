using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WinUSBNet;

namespace web_remote_usb.Server.Services;

public class UsbDevice : IDisposable
{
    string m_devicePath;
    int m_timeout;
    USBDevice? m_device;
    USBInterface? m_iface;

    public string DevicePath
    {
        get => m_devicePath;
    }

    public UsbDevice(
        string devicePath,
        int timeout)
    {
        m_devicePath = devicePath;
        m_timeout = timeout;
    }

    public void Open()
    {
        if (m_device != null ||
            m_iface != null)
        {
            throw new Exception("Already connected");
        }

        USBDevice device = new USBDevice(m_devicePath);
        m_iface = device.Interfaces.FirstOrDefault();

        if (m_iface == null)
        {
            throw new Exception("Unable to find usb device interface");
        }

        m_device = device;
    }

    public async Task<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken)
    {
        if (m_device == null ||
            m_iface == null)
        {
            throw new InvalidOperationException("USB device not connected");
        }

        CancellationTokenRegistration cancellationTokenReg =
            cancellationToken.Register(m_iface.InPipe.Abort);
        int result = 0;
        Task timeoutTask = Task.Delay(m_timeout);

        try
        {
            Task readTask = Task.Run(() =>
            {
                result = m_iface.InPipe.Read(buffer.Span);
            });

            await Task.WhenAny(timeoutTask, readTask);

            if (!readTask.IsCompleted)
            {
                m_iface.InPipe.Abort();
                throw new TimeoutException();
            }
        }
        finally
        {
            cancellationTokenReg.Dispose();
        }

        return result;
    }

    public async Task WriteAsync(Memory<byte> buffer, CancellationToken cancellationToken)
    {
        if (m_device == null ||
            m_iface == null)
        {
            throw new InvalidOperationException("USB device not connected");
        }

        CancellationTokenRegistration cancellationTokenReg =
            cancellationToken.Register(m_iface.OutPipe.Abort);
        Task timeoutTask = Task.Delay(m_timeout);

        try
        {
            Task writeTask = Task.Run(() =>
            {
                m_iface.OutPipe.Write(buffer.Span);
            });

            await Task.WhenAny(timeoutTask, writeTask);

            if (!writeTask.IsCompleted)
            {
                m_iface.OutPipe.Abort();
                throw new TimeoutException();
            }
        }
        finally
        {
            cancellationTokenReg.Dispose();
        }
    }

    public void Dispose()
    {
        if (m_device == null)
        {
            return;
        }

        m_device.Dispose();
        m_device = null;
        m_iface = null;
    }
}
