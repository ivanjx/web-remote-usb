using System;
using Microsoft.AspNetCore.Builder;

namespace web_remote_usb.Server.Controllers;

public class IndexController
{
    public static void Map(WebApplication app)
    {
        IndexController controller = new IndexController();
        app.MapGet("/", controller.Index);
    }

    public string Index()
    {
        return "hello world";
    }
}
