using System;
using System.Text.Json.Serialization;

namespace web_remote_usb.Server.Controllers;

[JsonSerializable(typeof(ExceptionHandlerMiddleware.ErrorResult))]
[JsonSerializable(typeof(string[]))]
[JsonSerializable(typeof(SerialPortController.SerialConnectRequest))]
[JsonSerializable(typeof(UsbController.UsbDeviceInfoResponse[]))]
[JsonSerializable(typeof(UsbController.UsbConnectRequest))]
public partial class ControllerJsonContext : JsonSerializerContext
{
}
