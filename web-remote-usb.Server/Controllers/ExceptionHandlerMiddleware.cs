using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Text.Json.Serialization;

namespace web_remote_usb.Server.Controllers;

public class ExceptionHandlerMiddleware : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next.Invoke(context);
        }
        catch (InsufficientParameterException ex)
        {
            Log.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    ex.Message,
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
        catch (Exception ex)
        {
            Log.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    ex.Message,
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
    }

    public record ErrorResult
    {
        [JsonPropertyName("error")]
        public string Error
        {
            get;
            set;
        }

        [JsonPropertyName("traceId")]
        public string TraceId
        {
            get;
            set;
        }

        public ErrorResult(string error, string traceId)
        {
            Error = error;
            TraceId = traceId;
        }
    }
}
