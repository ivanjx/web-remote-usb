using System;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using web_remote_usb.Server.Services;

namespace web_remote_usb.Server.Controllers;

public class UsbController
{
    public static void Map(WebApplication app)
    {
        UsbController controller = new UsbController(
            app.Services.GetRequiredService<IUsbService>());
        app.MapGet("/usb/list/{classGuid}", controller.ListAsync);
        app.MapPost("/usb/connect", controller.ConnectAsync);
        app.MapPost("/usb/read/{handle}", controller.ReadAsync);
        app.MapPost("/usb/write/{handle}", controller.WriteAsync);
        app.MapPost("/usb/disconnect/{handle}", controller.Disconnect);
    }

    IUsbService m_usbService;

    public UsbController(
        IUsbService usbService)
    {
        m_usbService = usbService;
    }

    public record UsbDeviceInfoResponse
    {
        [JsonPropertyName("path")]
        public string Path
        {
            get;
            set;
        }

        [JsonPropertyName("vid")]
        public ushort Vid
        {
            get;
            set;
        }

        [JsonPropertyName("pid")]
        public ushort Pid
        {
            get;
            set;
        }

        public UsbDeviceInfoResponse(UsbDeviceInfo info)
        {
            Path = info.Path;
            Vid = info.Vid;
            Pid = info.Pid;
        }
    }

    public async Task<UsbDeviceInfoResponse[]> ListAsync(
        [FromRoute] string classGuid)
    {
        UsbDeviceInfo[] devices = await m_usbService.ListAsync(classGuid);
        return devices
            .Select(x => new UsbDeviceInfoResponse(x))
            .ToArray();
    }

    public record UsbConnectRequest
    {
        [JsonPropertyName("path")]
        public string? Path
        {
            get;
            set;
        }

        [JsonPropertyName("timeout")]
        public int Timeout
        {
            get;
            set;
        }
    }

    public async Task<string> ConnectAsync(UsbConnectRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Path) ||
            request.Timeout == 0)
        {
            throw new InsufficientParameterException();
        }

        return await m_usbService.ConnectAsync(
            request.Path,
            request.Timeout);
    }

    public void Disconnect([FromRoute] string handle)
    {
        if (string.IsNullOrEmpty(handle))
        {
            throw new InsufficientParameterException();
        }

        m_usbService.Disconnect(handle);
    }

    public async Task ReadAsync(
        [FromRoute] string handle,
        [FromQuery(Name = "length")] int length,
        HttpContext context,
        CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(handle) ||
            length == 0)
        {
            throw new InsufficientParameterException();
        }

        byte[] result = await m_usbService.ReadAsync(
            handle,
            length,
            cancellationToken);
        await context.Response.Body.WriteAsync(
            result,
            cancellationToken);
    }

    public async Task WriteAsync(
        [FromRoute] string handle,
        HttpContext context,
        CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(handle))
        {
            throw new InsufficientParameterException();
        }

        byte[] data = new byte[context.Request.Body.Length];
        await context.Request.Body.ReadExactlyAsync(
            data,
            cancellationToken);
        await m_usbService.WriteAsync(
            handle,
            data,
            cancellationToken);
    }
}
