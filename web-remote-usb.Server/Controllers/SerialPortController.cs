using System;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using web_remote_usb.Server.Services;

namespace web_remote_usb.Server.Controllers;

public class SerialPortController
{
    public static void Map(WebApplication app)
    {
        SerialPortController controller = new SerialPortController(
            app.Services.GetRequiredService<ISerialPortService>());
        app.MapGet("/serial-port/list", controller.ListAsync);
        app.MapPost("/serial-port/connect", controller.ConnectAsync);
        app.MapPost("/serial-port/read/{handle}", controller.ReadAsync);
        app.MapPost("/serial-port/write/{handle}", controller.WriteAsync);
        app.MapPost("/serial-port/disconnect/{handle}", controller.Disconnect);
    }

    ISerialPortService m_serialPortService;

    public SerialPortController(
        ISerialPortService serialPortService)
    {
        m_serialPortService = serialPortService;
    }

    public async Task<string[]> ListAsync()
    {
        return await m_serialPortService.ListAsync();
    }

    public record SerialConnectRequest
    {
        [JsonPropertyName("name")]
        public string? Name
        {
            get;
            set;
        }

        [JsonPropertyName("baudRate")]
        public int BaudRate
        {
            get;
            set;
        }

        [JsonPropertyName("timeout")]
        public int Timeout
        {
            get;
            set;
        }
    }

    public async Task<string> ConnectAsync(SerialConnectRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Name) ||
            request.BaudRate == 0 ||
            request.Timeout == 0)
        {
            throw new InsufficientParameterException();
        }

        return await m_serialPortService.ConnectAsync(
            request.Name,
            request.BaudRate,
            request.Timeout);
    }

    public async Task ReadAsync(
        [FromRoute] string handle,
        [FromQuery(Name = "length")] int length,
        HttpContext context,
        CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(handle) ||
            length == 0)
        {
            throw new InsufficientParameterException();
        }

        byte[] result = await m_serialPortService.ReadAsync(
            handle,
            length,
            cancellationToken);
        await context.Response.Body.WriteAsync(
            result,
            cancellationToken);
    }

    public async Task WriteAsync(
        [FromRoute] string handle,
        HttpContext context,
        CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(handle))
        {
            throw new InsufficientParameterException();
        }

        byte[] data = new byte[context.Request.Body.Length];
        await context.Request.Body.ReadExactlyAsync(
            data,
            cancellationToken);
        await m_serialPortService.WriteAsync(
            handle,
            data,
            cancellationToken);
    }

    public void Disconnect([FromRoute] string handle)
    {
        if (string.IsNullOrEmpty(handle))
        {
            throw new InsufficientParameterException();
        }

        m_serialPortService.Disconnect(handle);
    }
}
