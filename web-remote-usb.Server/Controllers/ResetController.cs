using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using web_remote_usb.Server.Services;

namespace web_remote_usb.Server.Controllers;

public class ResetController
{
    public static void Map(WebApplication app)
    {
        ResetController controller = new ResetController(
            app.Services.GetRequiredService<ISerialPortService>(),
            app.Services.GetRequiredService<IUsbService>());
        app.MapPost("/reset", controller.Reset);
    }

    ISerialPortService m_serialPortService;
    IUsbService m_usbService;

    public ResetController(
        ISerialPortService serialPortService,
        IUsbService usbService)
    {
        m_serialPortService = serialPortService;
        m_usbService = usbService;
    }

    public void Reset()
    {
        m_serialPortService.DisconnectAll();
        m_usbService.DisconnectAll();
    }
}
