using System;

namespace web_remote_usb.Server.Controllers;

public class InsufficientParameterException : Exception
{
    public InsufficientParameterException() :
        base("Insufficient parameter")
    {
    }
}
