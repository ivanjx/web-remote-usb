import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class SerialPortStore {
    handle: string | undefined;
}
