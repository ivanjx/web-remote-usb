import { Injectable } from "@angular/core";
import { UsbStore } from "./usb-store";
import { config } from "./config";

export interface UsbDeviceInfo {
    path: string,
    vid: number,
    pid: number
}

@Injectable({
    providedIn: 'root'
})
export class UsbService {
    usbStore: UsbStore;

    constructor(usbStore: UsbStore) {
        this.usbStore = usbStore;
    }

    async list(classGuid: string): Promise<UsbDeviceInfo[]> {
        const response = await fetch(
            config.api + '/usb/list/' + classGuid,
            {
                method: 'GET'
            }
        );
        const responseData = await response.text();

        if (response.status !== 200) {
            throw new Error(responseData);
        }

        const usbs = JSON.parse(responseData);
        return usbs.map((x: any): UsbDeviceInfo => {
            return {
                path: String(x.path),
                vid: Number(x.vid),
                pid: Number(x.pid)
            };
        });
    }

    async connect(path: string, timeout: number) {
        if (this.usbStore.handle) {
            throw new Error('Already connected');
        }

        const response = await fetch(
            config.api + '/usb/connect',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    path: path,
                    timeout: timeout
                })
            }
        );
        const responseData = await response.text();

        if (response.status !== 200) {
            throw new Error(responseData);
        }

        this.usbStore.handle = responseData;
    }

    async disconnect() {
        if (!this.usbStore.handle) {
            throw new Error('Not connected');
        }

        await fetch(
            config.api + '/usb/disconnect/' + this.usbStore.handle,
            {
                method: 'POST'
            }
        );
        this.usbStore.handle = undefined;
    }

    async send(data: Uint8Array, responseLength: number): Promise<Uint8Array> {
        if (!this.usbStore.handle) {
            throw new Error('Not connected');
        }

        let response = await fetch(
            config.api + '/usb/write/' + this.usbStore.handle,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/octet-stream'
                },
                body: data
            }
        );

        if (response.status !== 200) {
            throw new Error(await response.text());
        }

        if (!responseLength) {
            return new Uint8Array([]);
        }

        response = await fetch(
            config.api + '/usb/read/' + this.usbStore.handle + '?length=' + responseLength,
            {
                method: 'POST'
            }
        );

        if (response.status !== 200) {
            throw new Error(await response.text());
        }

        const rawResponseData = await response.arrayBuffer();
        return new Uint8Array(rawResponseData);
    }
}
