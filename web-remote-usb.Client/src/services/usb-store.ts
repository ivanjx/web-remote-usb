import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class UsbStore {
    handle: string | undefined;
}
