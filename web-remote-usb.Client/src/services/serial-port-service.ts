import { Injectable } from "@angular/core";
import { SerialPortStore } from "./serial-port-store";
import { config } from "./config";

@Injectable({
    providedIn: 'root'
})
export class SerialPortService {
    serialPortStore: SerialPortStore;

    constructor(serialPortStore: SerialPortStore) {
        this.serialPortStore = serialPortStore;
    }

    async list(): Promise<string[]> {
        const response = await fetch(
            config.api + '/serial-port/list',
            {
                method: 'GET'
            }
        );
        const responseData = await response.text();

        if (response.status !== 200) {
            throw new Error(responseData);
        }

        const ports = JSON.parse(responseData);
        return ports.map((x: any) => String(x));
    }

    async connect(
        port: string,
        baudRate: number,
        timeout: number
    ) {
        if (this.serialPortStore.handle) {
            throw new Error('Already connected');
        }

        const response = await fetch(
            config.api + '/serial-port/connect',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: port,
                    baudRate: baudRate,
                    timeout: timeout
                })
            }
        );
        const responseData = await response.text();

        if (response.status !== 200) {
            throw new Error(responseData);
        }

        this.serialPortStore.handle = responseData;
    }

    async disconnect() {
        if (!this.serialPortStore.handle) {
            throw new Error('Not connected');
        }

        await fetch(
            config.api + '/serial-port/disconnect/' + this.serialPortStore.handle,
            {
                method: 'POST'
            }
        );
        this.serialPortStore.handle = undefined;
    }

    async send(data: string, responseLength: number): Promise<string> {
        if (!this.serialPortStore.handle) {
            throw new Error('Not connected');
        }

        const encodedData = new TextEncoder().encode(data);
        let response = await fetch(
            config.api + '/serial-port/write/' + this.serialPortStore.handle,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/octet-stream'
                },
                body: encodedData
            }
        );

        if (response.status !== 200) {
            throw new Error(await response.text());
        }

        if (!responseLength) {
            return '';
        }

        response = await fetch(
            config.api + '/serial-port/read/' + this.serialPortStore.handle + '?length=' + responseLength,
            {
                method: 'POST'
            }
        );

        if (response.status !== 200) {
            throw new Error(await response.text());
        }

        const rawResponseData = await response.arrayBuffer();
        return new TextDecoder().decode(rawResponseData);
    }
}
