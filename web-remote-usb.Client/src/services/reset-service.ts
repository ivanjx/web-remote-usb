import { Injectable } from "@angular/core";
import { config } from "./config";

@Injectable({
    providedIn: 'root'
})
export class ResetService {
    async reset() {
        await fetch(
            config.api + '/reset',
            {
                method: 'POST'
            }
        );
    }
}
