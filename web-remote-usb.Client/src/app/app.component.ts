import { Component, OnDestroy, OnInit, WritableSignal, computed, signal } from '@angular/core';
import { ResetService } from '../services/reset-service';
import { SerialPortService } from '../services/serial-port-service';
import { UsbDeviceInfo, UsbService } from '../services/usb-service';
import { SerialPortStore } from '../services/serial-port-store';
import { UsbStore } from '../services/usb-store';

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [],
    templateUrl: './app.component.html',
    styleUrl: './app.component.css'
})
export class AppComponent implements OnInit, OnDestroy {
    serialPortStore: SerialPortStore;
    usbStore: UsbStore;
    resetService: ResetService;
    serialPortService: SerialPortService;
    usbService: UsbService;

    mode: 'serial' | 'usb' = 'serial';
    requestStr: string = '';
    responseLen: number = 1024 * 1024; // 1 MB.
    responseStr: string = '';

    isLoading: WritableSignal<boolean> = signal(false);
    isConnected: WritableSignal<boolean> = signal(false);
    canNotChangeMode = computed(() =>
        this.isLoading() ||
        this.isConnected()
    );
    canNotSendRequest = computed(() =>
        this.isLoading() ||
        !this.isConnected()
    );

    ports: string[] = [];
    portBaudRate: number = 9600;
    selectedPort: string | undefined;
    usbs: UsbDeviceInfo[] = [];
    usbClassGuid: string = '';
    selectedUsb: UsbDeviceInfo | undefined;
    timeout: number = 10000;

    constructor(
        serialPortStore: SerialPortStore,
        usbStore: UsbStore,
        resetService: ResetService,
        serialPortService: SerialPortService,
        usbService: UsbService
    ) {
        this.serialPortStore = serialPortStore;
        this.usbStore = usbStore;
        this.resetService = resetService;
        this.serialPortService = serialPortService;
        this.usbService = usbService;
    }

    async ngOnInit() {
        await this.resetService.reset();
    }

    async ngOnDestroy() {
        await this.resetService.reset();
    }

    handleModeSerial() {
        this.mode = 'serial';
    }

    handleModeUsb() {
        this.mode = 'usb';
    }

    handlePortSelect(port: string) {
        this.selectedPort = port;
    }

    handleUsbSelect(usb: UsbDeviceInfo) {
        this.selectedUsb = usb;
    }

    handleSerialBaudChange(event: any) {
        if (!event.target.value) {
            this.portBaudRate = 0;
            return;
        }

        this.portBaudRate = Number(event.target.value);
    }

    handleUsbClassGuidChange(event: any) {
        this.usbClassGuid = String(event.target.value);
    }

    handleTimeoutChange(event: any) {
        if (!event.target.value) {
            this.timeout = 0;
            return;
        }

        this.timeout = Number(event.target.value);
    }

    handleRequestStrChange(event: any) {
        this.requestStr = event.target.value;
    }

    handleResponseLenChange(event: any) {
        if (!event.target.value) {
            this.portBaudRate = 0;
            return;
        }

        this.responseLen = Number(event.target.value);
    }

    async list() {
        this.isLoading.set(true);

        try {
            if (this.mode === 'serial') {
                this.ports = await this.serialPortService.list();
            } else if (this.mode == 'usb') {
                if (!this.usbClassGuid) {
                    alert('Please set the class guid first');
                    return;
                }

                this.usbs = await this.usbService.list(this.usbClassGuid);
            }
        } catch (ex: any) {
            alert('Error listing: ' + ex.message);
        } finally {
            this.isLoading.set(false);
        }
    }

    async connect() {
        this.isLoading.set(true);

        try {
            if (this.mode === 'serial') {
                if (!this.selectedPort ||
                    !this.portBaudRate ||
                    !this.timeout
                ) {
                    alert('Put all the fields correctly');
                    return;
                }

                await this.serialPortService.connect(
                    this.selectedPort,
                    this.portBaudRate,
                    this.timeout
                );
            } else if (this.mode == 'usb') {
                if (!this.selectedUsb ||
                    !this.timeout
                ) {
                    alert('Put all the fields correctly');
                    return;
                }

                await this.usbService.connect(
                    this.selectedUsb.path,
                    this.timeout
                );
            }

            this.isConnected.set(true);
        } catch (ex: any) {
            alert('Error connecting: ' + ex.message);
        } finally {
            this.isLoading.set(false);
        }
    }

    async disconnect() {
        this.isLoading.set(true);

        try {
            if (this.mode === 'serial') {
                await this.serialPortService.disconnect();
            } else if (this.mode == 'usb') {
                await this.usbService.disconnect();
            }

            this.isConnected.set(false);
        } catch (ex: any) {
            alert('Error connecting: ' + ex.message);
        } finally {
            this.isLoading.set(false);
        }
    }

    hexToUint8Array(hexString: string): Uint8Array {
        const length = hexString.length;
        let uint8Array = new Uint8Array(length / 2);
    
        for (let i = 0; i < length; i += 2) {
            uint8Array[i / 2] = parseInt(hexString.substring(i, 2), 16);
        }
    
        return uint8Array;
    }
    
    uint8ArrayToHex(uint8Array: Uint8Array): string {
        let hexString = '';
        uint8Array.forEach(byte => {
            hexString += ('0' + byte.toString(16)).slice(-2);
        });
    
        return hexString;
    }

    async send() {
        if (!this.requestStr) {
            alert('Nothing to be sent');
            return;
        }

        this.isLoading.set(true);
        this.responseStr = 'Sending data...';
        
        try {
            if (this.mode == 'serial') {
                this.responseStr = await this.serialPortService.send(
                    this.requestStr,
                    this.responseLen
                );
            } else if (this.mode == 'usb') {
                const result = await this.usbService.send(
                    this.hexToUint8Array(this.requestStr),
                    this.responseLen
                );
                this.responseStr = this.uint8ArrayToHex(result);
            }
        } catch (ex: any) {
            this.responseStr = 'Error = ' + ex.message;
        } finally {
            this.isLoading.set(false);
        }
    }
}
